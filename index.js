/*
PROBLEM1:
-----------------

Write a program that will do the following:

1. Require character input of either: O, X, Y, Z
2. Require non-negative odd integer input: eg. 3, 5, 7, etc.
3. Draw on the command line the character using *, the size should be the provided number.

Example:

> Enter a character [O, X, Y, Z]: Z
> Enter a non-negative odd integer: 7

*******
     *
    *
   *
  *
 *
*******

Note 1: If using an Object-oriented language, the use of Object-Oriented principles is a plus.
*/

console.log(`Tech Exam Second Attempt`);

function asciiLetters(){
	// AVAILABLE INPUTS
	const availLetters = ['O', 'X', 'Y', 'Z'];
	const availNums = [3, 5, 7, 9];

	// INPUT PROMPTS
	const inputLetter = (prompt(`Enter a character [O, X, Y, Z]: `)).toUpperCase();
	const inputNum = parseInt(prompt(`Enter a non-negative odd integer [3, 5, 7, etc.]: `));
	if(availLetters.includes(inputLetter) && (inputNum>2 && inputNum%2===1)){
		alert(`You chose ${inputLetter} and ${inputNum}`);
		// Conditions for letter O
		if(inputLetter==='O'){
			var strO = '';
			for(let i=1; i<inputNum+1; i++){
				for(let j=1; j<inputNum+1; j++){
					
					if(
						i+j===Math.ceil(inputNum*.34) || //TOP LEFT
						i-j===Math.round(inputNum*.67) || // BOTTOM LEFT
						j-i===Math.round(inputNum*.67) || // TOP RIGHT
						i+j===Math.round((inputNum*2)*.9) || // BOTTOM RIGHT LAST TO ACC
						(i===1 && (j>=Math.ceil(inputNum*.34) && j<=Math.ceil(inputNum*.67))) || // TOP
						(i===inputNum && (j>=Math.ceil(inputNum*.34) && j<=Math.ceil(inputNum*.67))) || // BOTTOM
						(j===1 && (i>=Math.ceil(inputNum*.34) && i<=Math.ceil(inputNum*.67))) || // LEFT
						(j===inputNum && (i>=Math.ceil(inputNum*.34) && i<=Math.ceil(inputNum*.67))) // RIGHT
						
						// OG CODE
						// i+j===Math.ceil(inputNum/2) ||
						// i*j===Math.ceil(inputNum/2) ||
						// j-i===Math.ceil(inputNum/2) ||
						// i-j===Math.ceil(inputNum/2) ||
						// i*j===inputNum*4 || // PROBLEMATIC SIDE
						// i+j===(inputNum-1)*2
						
					){
						strO += '*';
					} else {
						strO += ' ';
					}
				}
				strO += '\n';
			}
			console.log(strO);
		// Conditions for letter X
		} else if(inputLetter==='X'){
			var strX = '';
			for(let i=0; i<=inputNum-1; i++){
				for(let j=0; j<=inputNum-1; j++){
					if(i===j || i+j===inputNum-1){
						strX += '*';
					} else {
						strX += ' ';
					}
				}
				strX += '\n';
			}
			console.log(strX);
		// Conditions for letter Y
		} else if(inputLetter==='Y'){
			var strY = '';
			for(let i=0; i<inputNum; i++){
				for(let j=0; j<inputNum; j++){
					if(
						(i===j || i+j===(inputNum-1)) &&
						i<(Math.ceil(inputNum/2)) ||
						(j===(inputNum/2) && i>(inputNum/2)) ||
						(j===Math.floor(inputNum/2) && j-i<=-1) // Y TAIL FIX
					){
						
						strY += '*';
					} else {
						strY += ' ';
					}
				}
				strY += '\n';
			}
			console.log(strY);
		// Conditions for letter Z
		} else if(inputLetter==='Z'){
			var strZ = '';
			for(let i=0; i<inputNum; i++){
				for(let j=0; j<inputNum; j++){
					if(
						i===0 ||
						i===inputNum-1 ||	// FOR BOTTOM ROW FIX
						i+j===inputNum-1	// FOR DIAGONAL FIX
					){
						strZ += '*';
					} else {
						strZ += ' ';
					}
				}
				strZ += '\n';
			}
			console.log(strZ);
		}
	} else {
		alert(`One or more inputs were invalid.`)
	}
};

asciiLetters();
